import React from 'react';

const Character = ({data}) => {
  const { name, image, status } = data;
    return (
    <div className="character-card">
      <h2>{name}</h2>
      <img src={image} alt={name} />
      <p>Status: {status}</p>
    </div>
  );
};

export default Character;