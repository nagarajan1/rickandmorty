import React, { useEffect, useState } from "react";
import Character from "../../components/Character";

import FirstIcon from "../../assets/icons/First.png";
import PrevIcon from "../../assets/icons/Prev.png";
import NextIcon from "../../assets/icons/Next.png";
import LastIcon from "../../assets/icons/Last.png";
export default function HomePage() {
  const [data, setData] = React.useState([]);
  const [error, setError] = React.useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [pageinfo, setPageinfo] = useState({});
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  useEffect(() => {
    fetchRickAndMortyCharacters();
  }, [currentPage, name, status]);

  async function fetchRickAndMortyCharacters() {
    let url = "https://rickandmortyapi.com/api/character";

    // Add query parameters to the URL if provided
    if (name || status || currentPage) {
      url += '?';
      if (name) {
        url += `name=${name}`;
      }
      if (status) {
        url += `&status=${status}`;
      }
      if (currentPage) {
        url += `&page=${currentPage}`;
      }
    }
    try {
      const response = await fetch(url);
      const data = await response.json();
      setData(data.results);
      setPageinfo(data.info);
    } catch (error) {
      setError("Error fetching Rick and Morty characters");
    }
  }

  // Calculate pagination variables
  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;

  // Calculate page numbers
  const totalPages = (pageinfo && pageinfo.pages) || 0;
  const pageNumbers = Array.from(
    { length: totalPages },
    (_, index) => index + 1
  );

  // Handle page navigation
  const goToPage = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const goToPreviousPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const goToNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
    }
  };

  const goToFirstPage = () => {
    setCurrentPage(1);
  };

  const goToLastPage = () => {
    setCurrentPage(totalPages);
  };

  return (
    <div>
      <h1>Rick and Morty Characters</h1>
      <div>
        <input value={name} placeholder="Search" onChange={(e) => setName(e.target.value)} />
        <select id="status-select" value={status} onChange={(e) => setStatus(e.target.value)}>
        <option value="">All</option>
        <option value="alive">Alive</option>
        <option value="dead">Dead</option>
        <option value="unknown">Unknown</option>
      </select>
      </div>
      <ul className="character-list">
        {data && data.length>0?data.map((character) => (
          <li key={character.id}>
            {" "}
            <Character data={character} />
          </li>
        )):<p>No results</p>}
      </ul>
      {totalPages>0 &&<div className="pagination-wrapper">
        <div className="pagination-controls">
          <button
            className="first-btn page-btn"
            onClick={goToFirstPage}
            disabled={currentPage === 1}
          >
            <img src={FirstIcon} alt="firt-btn-icon" />
          </button>
          <button
            className="first-btn page-btn"
            onClick={goToPreviousPage}
            disabled={currentPage === 1}
          >
            <img src={PrevIcon} alt="prev-btn-icon" />
          </button>
          {pageNumbers.map((pageNumber, index) => (
            <>
            {pageNumber >= currentPage - 2 && pageNumber <= currentPage + 2 &&
              <button
                key={pageNumber}
                onClick={() => goToPage(pageNumber)}
                className={`page-btn ${
                  currentPage === pageNumber ? "page-btn-active" : ""
                }`}
              >
                {pageNumber}
              </button>}
            </>
          ))}
          <button
            className="first-btn page-btn"
            onClick={goToNextPage}
            disabled={currentPage === totalPages}
          >
            <img src={NextIcon} alt="next-btn-icon" />
          </button>
          <button
            className="first-btn page-btn"
            onClick={goToLastPage}
            disabled={currentPage === totalPages}
          >
            <img src={LastIcon} alt="last-btn-icon" />
          </button>
        </div>
      </div>}
      {error && <p>{error}</p>}
    </div>
  );
}
